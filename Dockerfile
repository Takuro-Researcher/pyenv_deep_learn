# FROM python:3.6.5
FROM nvidia/cuda:10.0-cudnn7-runtime


ARG proxy="133.44.62.221:8080"

ENV http_proxy http://${proxy}
ENV https_proxy http://${proxy}
ENV HTTP_PROXY http://${proxy}
ENV HTTPS_PROXY http://${proxy}

ENV no_proxy "http://133.44.75.132"
ENV NO_PROXY "http://133.44.75.132"
ENV DEBIAN_FRONTEND noninteractive

ENV LANG=ja_JP.UTF-8
ENV LANGUAGE=ja_JP:jp
ENV LC_ALL=ja_JP.UTF-8


RUN mkdir /workdir &&\
mkdir /workdir/mecabdic &&\
cd /workdir &&\
apt update &&\
# apt install  wget mecab libmecab-dev mecab-ipadic-utf8 swig git make curl xz-utils file  -y  
apt install  wget mecab libmecab-dev mecab-ipadic-utf8 swig git make curl xz-utils file python3 python3-pip python3-tk locales language-pack-ja-base language-pack-ja fonts-vlgothic fonts-horai-umefont fonts-umeplus libomp-dev -y

RUN locale-gen ja_JP.UTF-8

RUN ln -s /usr/bin/python3  /usr/bin/python
RUN ln -s /usr/bin/pip3  /usr/bin/pip
ADD ./pip_install_list.txt /workdir
RUN pip install --upgrade setuptools --proxy=${proxy}
RUN pip install --upgrade pip --proxy=${proxy}
RUN pip install -r /workdir/pip_install_list.txt --proxy=${proxy}
COPY ./mecab/mecabdic /workdir/mecabdic
COPY ./mecab/mecabrc /etc
COPY ./jupyter_notebook_config.py /root/.jupyter/

RUN  mkdir /workdir/CRF++-0.58
COPY ./CRF++-0.58 /workdir/CRF++-0.58
WORKDIR /workdir/CRF++-0.58
RUN ./configure &&\
    make &&\
    make install &&\
    ldconfig

RUN  mkdir /workdir/cabocha-0.69
COPY ./cabocha-0.69 /workdir/cabocha-0.69
WORKDIR /workdir/cabocha-0.69
RUN chmod +x configure &&\
    sync &&\
    ./configure --with-mecab-config=`which mecab-config` --with-charset=UTF8 &&\
    # ./configure  --with-charset=UTF8 &&\
    make &&\
    # make check &&\
    make install  &&\
    ldconfig

RUN apt install python3-dev -y &&\
    swig -python -shadow -c++ swig/CaboCha.i &&\
    mv swig/CaboCha.py python/ &&\
    mv swig/CaboCha_wrap.cxx python/
WORKDIR /workdir/cabocha-0.69/python
RUN python3 setup.py build_ext &&\
    python3 setup.py install &&\
    ldconfig

COPY ./searchtweets/result_stream.py /usr/local/lib/python3.6/dist-packages/searchtweets/
COPY ./scipy/ltisys.py /usr/local/lib/python3.6/dist-packages/scipy/signal/


# install nkf
WORKDIR /workdir/
RUN wget 'https://ja.osdn.net/frs/redir.php?m=jaist&f=nkf%2F64158%2Fnkf-2.1.4.tar.gz' -O nkf-2.1.4.tar.gz &&\
    tar zxvf nkf-2.1.4.tar.gz 
WORKDIR /workdir/nkf-2.1.4
RUN make &&\
    make install

RUN pip install ipywidgets &&\ 
curl -sL https://deb.nodesource.com/setup_12.x | bash - &&\
apt -y install nodejs 

# autocompete設定だが、現状は軽量なKiteを優先
RUN yes Y | bash -c "$(wget -q -O - https://linux.kite.com/dls/linux/current)" &&\
pip install jupyter-kite &&\
jupyter labextension install "@kiteco/jupyterlab-kite"

CMD tail -f /dev/null

# jlab ポート番号 で起動する
RUN echo "function jlab() { \n  command jupyter lab --ip=0.0.0.0 --port=\$1 --allow-root \n}" >> ~/.bashrc
RUN echo "function allfind() { \n command find \$1 -type f | grep .\$2\$ | xargs grep -n \$3 \n}" >> ~/.bashrc

# code整形用ツール
RUN jupyter labextension install @ryantam626/jupyterlab_code_formatter &&\
pip install jupyterlab_code_formatter &&\
jupyter serverextension enable --py jupyterlab_code_formatter &&\
pip install black isort

# tqdmなどアニメーションが必要な奴
RUN jupyter nbextension enable --py --sys-prefix widgetsnbextension &&\
jupyter labextension install @jupyter-widgets/jupyterlab-manager

# 高橋さんコンテナに合わせるバージョン
RUN pip install  -U ipykernel==5.1.2 &&\
pip install -U ipython==7.8.0 &&\
pip install -U jupyter-client==5.3.4 &&\
pip install -U jupyter-console==6.0.0 &&\
pip install -U tornado==6.0.3  

