FROM python:3.6.5


ARG proxy="proxy.nagaokaut.ac.jp:8080"
ARG exefile="test.py"

ENV EXE_FILE_PATH /Data/src/
ENV EXE_FILE test.py
ENV http_proxy http://${proxy}
ENV https_proxy http://${proxy}
ENV HTTP_PROXY http://${proxy}
ENV HTTPS_PROXY http://${proxy}

RUN mkdir /workdir &&\
cd /workdir
#     mkdir /Data
ADD ./pip_install_list.txt /workdir
# ADD ./src /Data
# 
# WORKDIR /workdir 
RUN pip install --upgrade pip --proxy=${proxy}
RUN pip install -r /workdir/pip_install_list.txt --proxy=${proxy}
#CMD ["/usr/bin/python3.5" ,exefile ]
CMD tail -f /dev/null


